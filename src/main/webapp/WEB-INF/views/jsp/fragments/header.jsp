<%--
  Created by IntelliJ IDEA.
  User: ahs
  Date: 12.06.16
  Time: 19:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="row">
            <div id="header-linker" class="col-sm-4">
                <a href="/weathertask" class="navbar-brand"><fmt:message key="app.title"/></a>
            </div>

        </div>
    </div>
</div>