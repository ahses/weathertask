<%--
  Created by IntelliJ IDEA.
  User: ahs
  Date: 12.06.16
  Time: 20:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<jsp:include page="fragments/headTag.jsp"/>

<body onload="afterLoad()">
<jsp:include page="fragments/header.jsp"/>


<div class="jumbotron">
    <div class="col-sm-5">
        <div class="container">

            <div class="shadow">
                <div class="view-box">
                    <form method="POST" id="cityfinder" action="javascript:void(null);" onsubmit="findClick()"
                          onreset="resetClick()">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="reqCity" class="control-label col-xs-2"><fmt:message
                                        key="app.city"/> </label>

                                <div class="col-xs-4">
                                    <input type="text" class="form-control" id="reqCity" name="reqCity"
                                           placeholder="city">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-offset-3">
                                    <button type="submit" class="btn btn-primary pull-left"><fmt:message
                                            key="app.getweather"/></button>
                                </div>
                                <div class="col-xs-offset-3">
                                    <button type="reset" class="btn btn-danger pull-right"><fmt:message
                                            key="app.reset"/></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row" id="rainbow">
                        <div id="fountainTextG">
                            <div id="fountainTextG_1" class="fountainTextG">L</div>
                            <div id="fountainTextG_2" class="fountainTextG">o</div>
                            <div id="fountainTextG_3" class="fountainTextG">a</div>
                            <div id="fountainTextG_4" class="fountainTextG">d</div>
                            <div id="fountainTextG_5" class="fountainTextG">i</div>
                            <div id="fountainTextG_6" class="fountainTextG">n</div>
                            <div id="fountainTextG_7" class="fountainTextG">g</div>
                            <div id="fountainTextG_8" class="fountainTextG">.</div>
                            <div id="fountainTextG_9" class="fountainTextG">.</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-sm-7">
        <div class="container">
            <div class="shadow">
                <div class="row">
                    <label for="cityResult" class="control-label col-xs-4"><fmt:message key="app.city"/> </label>
                    <div class="col-xs-2">
                        <div id="cityResult" class="results"></div>
                    </div>
                </div>
                <div class="row">
                    <label for="countryResult" class="control-label col-xs-4"><fmt:message key="app.country"/> </label>
                    <div class="col-xs-2">
                        <div id="countryResult" class="results "></div>
                    </div>
                </div>
                <div class="row">
                    <label for="tempResult" class="control-label col-xs-4"><fmt:message key="app.temp"/> </label>
                    <div class="col-xs-2">
                        <div id="tempResult" class="results"></div>
                    </div>
                </div>
                <div class="row">
                    <label for="windResult" class="control-label col-xs-4"><fmt:message key="app.wind"/> </label>
                    <div class="col-xs-2">
                        <div id="windResult" class="results"></div>
                    </div>
                </div>
                <div class="row">
                    <label for="rainResult" class="control-label col-xs-4"><fmt:message key="app.rain"/> </label>
                    <div class="col-xs-2">
                        <div id="rainResult" class="results"></div>
                    </div>
                </div>
                <div class="row">
                    <label for="humidResult" class="control-label col-xs-4"><fmt:message key="app.humidity"/> </label>
                    <div class="col-xs-2">
                        <div id="humidResult" class="results"></div>
                    </div>
                </div>
                <div class="row">
                    <label for="pressResult" class="control-label col-xs-4"><fmt:message key="app.pressure"/> </label>
                    <div class="col-xs-2">
                        <div id="pressResult" class="results"></div>
                    </div>
                </div>
                <div class="row">
                    <label for="cloudsResult" class="control-label col-xs-4"><fmt:message key="app.cloudy"/> </label>
                    <div class="col-xs-2">
                        <div id="cloudsResult" class="results"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<jsp:include page="fragments/footer.jsp"/>


</body>
</html>
