var findWeather;
var ajaxUrlCity = 'ajax/weather/getCityWeather'

function findClick() {
    var $form = $('#cityfinder');
    if (!$('#reqCity').val()) {
        failNotySimple('Enter city name!');
    } else
        $.ajax({
            type: "POST",
            url: ajaxUrlCity,
            data: $form.serialize(),
            success: function (data) {
                $('#cityResult').empty().append(data.city);
                $('#countryResult').empty().append(data.country);
                $('#tempResult').empty().append(data.temp);
                $('#windResult').empty().append(data.wind);
                $('#rainResult').empty().append(data.rain);
                $('#humidResult').empty().append(data.humidity);
                $('#pressResult').empty().append(data.pressure);
                $('#cloudsResult').empty().append(data.cloudy);
            }
        });
}

function resetClick() {
    $('#reqCity').empty();
    $('#cityResult').empty();
    $('#countryResult').empty();
    $('#tempResult').empty();
    $('#windResult').empty();
    $('#rainResult').empty();
    $('#humidResult').empty();
    $('#pressResult').empty();
    $('#cloudsResult').empty();

}


var reqErr;

$.ajaxSetup({
    url: ajaxUrlCity, // путь к php-обработчику
    type: 'POST', // метод передачи данных
    dataType: 'json', // тип ожидаемых данных в ответе
    beforeSend: function () { // Функция вызывается перед отправкой запроса
        reqErr = false;
        successNoty('Request has been sent. Wait for an answer.');
        $('#rainbow').show();
    },
    error: function (req, text, error) { // отслеживание ошибок во время выполнения ajax-запроса
        reqErr = true;
        failNotySimple('Error in request! ' + text + ' | ' + error);
        rainbowStop();

    },
    complete: function () { // функция вызывается по окончании запроса
        if (!reqErr)
            successNoty('Request is completed');
        rainbowStop();

    }
});


var failedNote;

function closeNoty() {
    if (failedNote) {
        failedNote.close();
        failedNote = undefined;
    }
}

function successNoty(text) {
    closeNoty();
    noty({
        text: text,
        type: 'success',
        layout: 'bottomRight',
        timeout: 1500
    });
}

function failNotySimple(msg) {
    closeNoty();
    failedNote = noty({
        text: 'Failed: ' + msg,
        type: 'error',
        layout: 'bottomRight',
        timeout: 3000
    });
}

function failNoty(event, jqXHR, options, jsExc) {
    closeNoty();
    var errorInfo = $.parseJSON(jqXHR.responseText);
    failedNote = noty({
        text: 'Failed: ' + jqXHR.statusText + "<br>" + errorInfo.cause + "<br>" + errorInfo.detail,
        type: 'error',
        layout: 'bottomRight'
    });
}

function afterLoad() {
    rainbowStop();

}

function rainbowStop() {
    $('#rainbow').hide();

}
