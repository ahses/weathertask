package es.ahs.weathertask.service;

import es.ahs.weathertask.model.Weather;
import es.ahs.weathertask.model.WeatherRequest;
import net.aksingh.owmjapis.CurrentWeather;
import net.aksingh.owmjapis.OpenWeatherMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.io.IOException;


/**
 * Created by ahs on 12.06.16.
 */

@Service
public class WeatherService {
    private static final Logger log = LoggerFactory.getLogger(WeatherService.class);
    static int counter = 0;

    public Weather getWeatherByCityName(WeatherRequest weatherRequest) {

        Weather w = new Weather();
        if (weatherRequest == null || weatherRequest.getReqCity() == null) return w;
        String city = weatherRequest.getReqCity();

        // declaring object of "OpenWeatherMap" class
        OpenWeatherMap owm = new OpenWeatherMap(OpenWeatherMap.Units.METRIC, OpenWeatherMap.Language.RUSSIAN, "6d2027dab11ca257dde06f8b714380ef");

        CurrentWeather cwd = null;
        try {
            cwd = owm.currentWeatherByCityName(city);
        } catch (IOException e) {
            w.setCity("error-city");
        }
        w.setId(++counter);
        w.setCity(cwd.getCityName());
        w.setTemp(cwd.getMainInstance().getTemperature() + "");
        w.setWind(cwd.hasWindInstance() ? cwd.getWindInstance().getWindSpeed() + "" : "no data");
        w.setRain(cwd.hasRainInstance() ? cwd.getRainInstance().getRain() + "" : "no data");
        w.setCoord(cwd.hasCoordInstance() ? "Lat: " + cwd.getCoordInstance().getLatitude() +
                                            "  Lng: " + cwd.getCoordInstance().getLongitude() : "...");
        w.setPressure(cwd.getMainInstance().getPressure() + "");
        w.setHumidity(cwd.getMainInstance().getHumidity() + "");
        w.setCountry((cwd.hasSysInstance() && cwd.getSysInstance().hasCountryCode()) ? cwd.getSysInstance().getCountryCode() : "no data");
        w.setCloudy(cwd.hasCloudsInstance() ? cwd.getCloudsInstance().getPercentageOfClouds() + "" : "no data");
        return w;
    }
}
