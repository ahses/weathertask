package es.ahs.weathertask.controller;

import es.ahs.weathertask.model.Weather;
import es.ahs.weathertask.model.WeatherRequest;
import es.ahs.weathertask.service.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
/**
 * Created by ahs on 12.06.16.
 */
@RestController
@RequestMapping(value = "/ajax/weather")
public class AjaxController {
    private static final Logger log = LoggerFactory.getLogger(AjaxController.class);
    @Autowired
    WeatherService service;

    @RequestMapping(value = "/getCityWeather", produces = MediaType.APPLICATION_JSON_VALUE)
    public Weather getCityWeather(@Valid WeatherRequest wreq, BindingResult result) {
        if (result.hasErrors()) {
            log.info("ERROR!!! wreq = " + wreq);
        }
        log.info("wreq = " + wreq);
        return service.getWeatherByCityName(wreq);
    }
}
