package es.ahs.weathertask.controller;

import es.ahs.weathertask.service.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class RootController {

    private final Logger logger = LoggerFactory.getLogger(RootController.class);

    @Autowired
    WeatherService weatherService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String weatherRoot() {
        return "getWeather";
    }

    @RequestMapping(value = "/getWeather", method = RequestMethod.GET)
    public String jsonweather() {
        return "getWeather";
    }
}