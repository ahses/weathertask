package es.ahs.weathertask.model;

import org.springframework.stereotype.Component;

/**
 * Created by ahs on 12.06.16.
 */
@Component
public class WeatherRequest {
    private String reqCity;
    private String reqCountry;
    private String days;

    public WeatherRequest() {
    }

    public String getReqCity() {
        return reqCity;
    }

    public void setReqCity(String reqCity) {
        this.reqCity = reqCity;
    }

    public String getReqCountry() {
        return reqCountry;
    }

    public void setReqCountry(String reqCountry) {
        this.reqCountry = reqCountry;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    @Override
    public String toString() {
        return "WeatherRequest{" +
                " reqCity='" + reqCity + '\'' +
                ", reqCountry='" + reqCountry + '\'' +
                ", days='" + days + '\'' +
                '}';
    }
}
